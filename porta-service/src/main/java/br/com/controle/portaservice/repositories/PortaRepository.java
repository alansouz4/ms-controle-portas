package br.com.controle.portaservice.repositories;

import br.com.controle.portaservice.models.Porta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PortaRepository extends JpaRepository<Porta, Long> {
}
