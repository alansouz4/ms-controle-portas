package br.com.controle.portaservice.gateways;

import br.com.controle.portaservice.models.Porta;

public interface PortaGateway {

    Porta criaPorta(Porta porta);
    Porta buscaPorta(Long id);
}
