package br.com.controle.portaservice.services;

import br.com.controle.portaservice.exceptions.PortaNotFoundException;
import br.com.controle.portaservice.gateways.PortaGateway;
import br.com.controle.portaservice.models.Porta;
import br.com.controle.portaservice.repositories.PortaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PortaService implements PortaGateway {

    private final PortaRepository portaRepository;

    @Override
    public Porta criaPorta(Porta porta) {
        return portaRepository.save(porta);
    }

    @Override
    public Porta buscaPorta(Long id) {
        Optional<Porta> portaOptional = portaRepository.findById(id);
        if ((portaOptional.isPresent())) {
            return portaOptional.get();
        }
        throw new PortaNotFoundException();
    }
}
