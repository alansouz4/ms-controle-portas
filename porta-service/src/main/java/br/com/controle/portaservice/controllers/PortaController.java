package br.com.controle.portaservice.controllers;

import br.com.controle.portaservice.models.Porta;
import br.com.controle.portaservice.services.PortaService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/porta")
@RequiredArgsConstructor
public class PortaController {

    private final PortaService portaService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Porta salvaPorta(@Valid @RequestBody Porta porta) {
        return portaService.criaPorta(porta);
    }

    @GetMapping("/{id}")
    private Porta listaPorta(@PathVariable Long id) {
        return portaService.buscaPorta(id);
    }

}
