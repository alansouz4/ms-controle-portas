package br.com.controle.acessoservice.models.mappers;

import br.com.controle.acessoservice.models.Acesso;
import br.com.controle.acessoservice.models.request.AcessoRequest;
import br.com.controle.acessoservice.models.responses.AcessoResponse;
import org.springframework.stereotype.Component;

@Component
public class AcessoMapper {

    public AcessoResponse toAcessoResponse(Acesso acesso) {

        AcessoResponse acessoResponse = new AcessoResponse();

        acessoResponse.setCliente_id(acesso.getClienteId());
        acessoResponse.setPorta_id(acesso.getPortaId());

        return acessoResponse;
    }

    public Acesso toAcesso(AcessoRequest acessoRequest) {

        Acesso acesso = new Acesso();

        acesso.setClienteId(acessoRequest.getCliente_id());
        acesso.setPortaId(acessoRequest.getPorta_id());

        return acesso;
    }
}
