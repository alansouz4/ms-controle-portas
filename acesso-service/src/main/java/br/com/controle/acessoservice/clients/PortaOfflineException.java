package br.com.controle.acessoservice.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "O serviço de porta esta offline")
public class PortaOfflineException extends RuntimeException{
}
