package br.com.controle.acessoservice.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PortaClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new PortaClientDecoder();
    }
}
