package br.com.controle.acessoservice.services;

import br.com.controle.acessoservice.clients.ClienteClient;
import br.com.controle.acessoservice.clients.PortaClient;
import br.com.controle.acessoservice.exception.AcessoNotfoundException;
import br.com.controle.acessoservice.gateways.AcessoGateway;
import br.com.controle.acessoservice.models.Acesso;
import br.com.controle.acessoservice.models.modelsclients.Cliente;
import br.com.controle.acessoservice.models.modelsclients.Porta;
import br.com.controle.acessoservice.repositories.AcessoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AcessoService implements AcessoGateway {

    private final AcessoRepository acessoRepository;
    private final PortaClient portaClient;
    private final ClienteClient clienteClient;

    @Override
    public Acesso criaAcesso(Acesso acesso) {
        Porta porta = portaClient.listaPorta(acesso.getPortaId());
        Cliente cliente = clienteClient.buscaCliente(acesso.getClienteId());

        acesso.setPortaId(porta.getId());
        acesso.setClienteId(cliente.getId());

        return acessoRepository.save(acesso);
    }

    @Override
    public Acesso consultaAcesso(Long clienteId, Long portaId) {
        Cliente cliente = clienteClient.buscaCliente(clienteId);
        Porta porta = portaClient.listaPorta(portaId);

        Optional<Acesso> acessoOptional = acessoRepository.findByClienteIdAndPortaId(cliente.getId(), porta.getId());
        if(acessoOptional.isPresent()) {
            return acessoOptional.get();
        }
        throw new AcessoNotfoundException();
    }

    @Override
    public void deletaAcesso(Long clienteId, Long portaId) {
        Cliente cliente = clienteClient.buscaCliente(clienteId);
        Porta porta = portaClient.listaPorta(portaId);
        Optional<Acesso> acessoOptional = acessoRepository.findByClienteIdAndPortaId(cliente.getId(), porta.getId());
        if (acessoOptional.isPresent()) {
            acessoRepository.deleteByClienteIdAndPortaId(cliente.getId(), porta.getId());
        } else {
            throw new AcessoNotfoundException();
        }
    }
}
