package br.com.controle.acessoservice.services;

import br.com.controle.acessoservice.models.Acesso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class AcessoProducer {

    @Autowired
    private KafkaTemplate<String, Acesso> producer;

    public void enviarAoKafka(Acesso acesso) {
        producer.send("spec3-alan-augusto-2", acesso);
    }

}
