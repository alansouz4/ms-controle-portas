package br.com.controle.acessoservice.models.responses;

import lombok.Data;

@Data
public class AcessoResponse {

    private Long cliente_id;
    private Long porta_id;
}
