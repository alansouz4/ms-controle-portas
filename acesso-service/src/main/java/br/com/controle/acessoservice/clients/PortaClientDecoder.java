package br.com.controle.acessoservice.clients;

import feign.Response;
import feign.codec.ErrorDecoder;

public class PortaClientDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new InvalidPortaException();
        }else {
            return errorDecoder.decode(s, response);
        }
    }
}
