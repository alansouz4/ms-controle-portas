package br.com.controle.acessoservice.clients;

import br.com.controle.acessoservice.models.modelsclients.Cliente;

public class ClienteClientFallback implements ClienteClient{

    @Override
    public Cliente buscaCliente(Long id) {

//        Cliente cliente = new Cliente();
//        cliente.setId(1L);
//        cliente.setNome("Alan Augusto");
//
//        return cliente;
        throw new ClienteOfflineException();
    }
}
