package br.com.controle.acessoservice.models.request;

import lombok.Data;

@Data
public class AcessoRequest {

    private Long cliente_id;
    private Long porta_id;
}
