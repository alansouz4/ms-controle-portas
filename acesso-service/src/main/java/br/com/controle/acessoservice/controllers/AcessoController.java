package br.com.controle.acessoservice.controllers;

import br.com.controle.acessoservice.models.Acesso;
import br.com.controle.acessoservice.models.mappers.AcessoMapper;
import br.com.controle.acessoservice.models.request.AcessoRequest;
import br.com.controle.acessoservice.models.responses.AcessoResponse;
import br.com.controle.acessoservice.services.AcessoProducer;
import br.com.controle.acessoservice.services.AcessoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
@RequiredArgsConstructor
public class AcessoController {

    private final AcessoService acessoService;
    private final AcessoMapper mapper;
    private final AcessoProducer acessoProducer;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcessoResponse salvaAcesso(@Valid @RequestBody AcessoRequest acessoRequest) {

        Acesso acesso = mapper.toAcesso(acessoRequest);
        acesso = acessoService.criaAcesso(acesso);

        acessoProducer.enviarAoKafka(acesso);

        return mapper.toAcessoResponse(acesso);
    }

    @GetMapping("/{clienteId}/{portaId}")
    public AcessoResponse buscaAcesso(@PathVariable Long clienteId, @PathVariable Long portaId) {

        Acesso acesso = new Acesso();
        acesso = acessoService.consultaAcesso(clienteId, portaId);

        return mapper.toAcessoResponse(acesso);
    }

    @DeleteMapping("/{clienteId}/{portaId}")
    public ResponseEntity<?> deletaAcesso(@PathVariable Long clienteId, @PathVariable Long portaId) {
        acessoService.deletaAcesso(clienteId, portaId);
        return ResponseEntity.status(204).body("");
    }
}
