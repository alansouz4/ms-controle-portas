package br.com.controle.acessoservice.models.modelsclients;

import lombok.Data;

@Data
public class Porta {

    private Long id;
    private String andar;
    private String sala;
}
