package br.com.controle.acessoservice.models.modelsclients;

import lombok.Data;

@Data
public class Cliente {

    private Long id;
    private String nome;
}
