package br.com.controle.acessoservice.clients;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "O cliente informado é invalido")
public class InvalidClienteException extends RuntimeException{
}
