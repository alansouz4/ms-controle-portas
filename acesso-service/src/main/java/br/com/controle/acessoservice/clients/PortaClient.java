package br.com.controle.acessoservice.clients;

import br.com.controle.acessoservice.models.modelsclients.Porta;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "porta", configuration = PortaClientConfiguration.class)
public interface PortaClient {

    @GetMapping("/porta/{id}")
    Porta listaPorta(@PathVariable Long id);

}
