package br.com.controle.acessoservice.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Data
@Entity
@JsonIgnoreProperties(value = "id", allowSetters = true)
public class Acesso {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Porta é obrigatório")
    private Long portaId;

    @NotNull(message = "Cliente é obrigatório")
    private Long clienteId;
}
