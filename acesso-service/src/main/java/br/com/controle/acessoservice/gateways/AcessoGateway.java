package br.com.controle.acessoservice.gateways;

import br.com.controle.acessoservice.models.Acesso;

public interface AcessoGateway {

    Acesso criaAcesso(Acesso acesso);
    Acesso consultaAcesso(Long clienteId, Long portaId);
    void deletaAcesso(Long clienteId, Long portaId);

}
