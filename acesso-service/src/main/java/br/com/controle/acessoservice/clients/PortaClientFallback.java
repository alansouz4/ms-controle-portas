package br.com.controle.acessoservice.clients;

import br.com.controle.acessoservice.models.modelsclients.Porta;

public class PortaClientFallback implements PortaClient{

    @Override
    public Porta listaPorta(Long id) {

//        Porta porta = new Porta();
//
//        porta.setId(1L);
//        porta.setAndar("8°");
//        porta.setSala("10A");
//
//        return porta;

        throw new PortaOfflineException();
    }
}
