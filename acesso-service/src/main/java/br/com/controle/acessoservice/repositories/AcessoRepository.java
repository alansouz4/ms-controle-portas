package br.com.controle.acessoservice.repositories;

import br.com.controle.acessoservice.models.Acesso;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AcessoRepository extends JpaRepository<Acesso, Long> {

    Optional<Acesso> findByClienteIdAndPortaId(Long clienteId, Long portaId);
    Optional<Acesso> deleteByClienteIdAndPortaId(Long clienteId, Long portaId);
}
