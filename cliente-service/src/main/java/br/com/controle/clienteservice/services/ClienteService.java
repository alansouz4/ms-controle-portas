package br.com.controle.clienteservice.services;

import br.com.controle.clienteservice.exceptions.ClienteNotFoundException;
import br.com.controle.clienteservice.gateways.ClienteGateway;
import br.com.controle.clienteservice.models.Cliente;
import br.com.controle.clienteservice.repositories.ClienteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ClienteService implements ClienteGateway {

    private final ClienteRepository clienteRepository;

    @Override
    public Cliente criaCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    @Override
    public Cliente buscaCliente(Long id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if(clienteOptional.isPresent()){
            return clienteOptional.get();
        }

        throw new ClienteNotFoundException();
    }
}
