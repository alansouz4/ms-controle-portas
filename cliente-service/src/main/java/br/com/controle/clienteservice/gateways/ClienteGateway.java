package br.com.controle.clienteservice.gateways;

import br.com.controle.clienteservice.models.Cliente;

public interface ClienteGateway {

    Cliente criaCliente(Cliente cliente);
    Cliente buscaCliente(Long id);
}
