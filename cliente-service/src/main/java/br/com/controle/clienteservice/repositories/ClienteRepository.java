package br.com.controle.clienteservice.repositories;

import br.com.controle.clienteservice.models.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
}
