package br.com.controle.clienteservice.controllers;

import br.com.controle.clienteservice.models.Cliente;
import br.com.controle.clienteservice.services.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
@RequiredArgsConstructor
public class ClienteController {

    private final ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente salvarCliente(@Valid @RequestBody Cliente cliente) {
        return clienteService.criaCliente(cliente);
    }

    @GetMapping("/{id}")
    public Cliente listaCliente(@PathVariable Long id) {
        return clienteService.buscaCliente(id);
    }
}
