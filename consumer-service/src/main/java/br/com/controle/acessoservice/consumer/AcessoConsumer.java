package br.com.controle.acessoservice.consumer;

import br.com.controle.acessoservice.models.Acesso;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-alan-augusto-2", groupId = "controle-acesso-1")
    public void receber(@Payload Acesso acesso) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        System.out.println("Recebi um acesso do cliente" + acesso.getClienteId()
                + " na porta " + acesso.getPortaId());

        Writer writer = new FileWriter("D:\\Documentos\\Documentos\\myprojects\\micro-services-controle-portas\\acesso.csv", true);
        StatefulBeanToCsv<Acesso> beanToCsv = new StatefulBeanToCsvBuilder(writer).build();

        beanToCsv.write(acesso);

        writer.flush();
        writer.close();
    }
}
